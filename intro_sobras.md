
INTRO 

SAQUE 
@Mccoy1990 supported his proposition with data of insects from low
and high altitudes. The insects at lower altitudes and warmer conditions had
shorter life cycles compare to insects from higher altitudes. Then, at lower
altitudes a rather short sampling could only capture a small part of the total
richness.

FALTA 
CONSERVATION
Another posibility of a spurios mid elevation pattern is a much higher
antropogenic impact in the forest at lower altitudes (ref.). This is true in
the Kosnipata valley, and the reason why I choose to sample in the adjacent
and protected forest of Tono valley. Antropogenic discussion ,,,
conservation.

FALTA
Some ecological hypothesis to explain the altitudinal richness pattern are the
community overlap theory [@Lomolino2001], the relationship between
productivity and richness as an extention of the species-energy theory
[@Rosenzweig1995] and areas of optimal ecological condition [@Mccain2004]. WHAT THEY ARE BASE ON ,, LOMOLINO


. Mountain diversity called de attention of the earliest researches,
elevation gradients were used in biogeography and evolution studies by
Humboldt and Darwing with implicances on studies of latitudinal distribution
of species .

Other particularities determining the seasonality of the insect under study
might reside on some stages of their life history such as the development rate
of inmature stages, the longevity of adults, the synchronization of males, the
number of generations per year [@Wolda1988] and the length of their diapausing
period [@Gauld1987]. In the case of parasitoid wasps, the studies of
seasonality patterns ussualy focus on the adult stage. However, the other stages
and the mechanisms controlling them might influence also in the observed
patterns. Those characteristics are ussualy evaluated on parasitoids that are
employed or that have potential as pest controls but, not in studies of
pristine or secundary tropical forest. FALTA COMPLETAR CON BARTLETT

Spatial and temporal structure of ecosystems are an important component of
ecological theories [@Legendre1989] and a fundamental feature of tropical
insect communities [@Devries1997; @Devries1999]. 

FALTO
Tropical biodiversity studies of lowland rainforest over montane rainforest forest specially in arthropods eventhough this areas are recognize as a "hyper hotspot" on plants and vertebrates richness [@Brehm2003].

Temporal variation of species
reflects ecological processes and are key to understand diversity
[@Rosenzweig1995]. Therefore, studies of temporal variation should be
complementary to studies of local richness and geographical patterns studies
[@White2010] however, temporal variation is an underrated component of
diversity. It is perceived as a trivial question for temperate areas and for
tropical areas the documentation is scarce [@Rosenzweig1995]. In the case of
parasitic wasps, their potential to be used as pest control in agriculture has
promoted the study of economically important species [@Stireman2002] but
mainly in temperate regions. There are no/scarse/number studies of temporal variation focusing on tropical /montane Pimplinae or Ichneumonidae. 

FALTO
The role of environment and mid-domain effect on moth species richness along a tropical elevational gradient @Brehm2007
Geometridae and Arcti- idae in Borneo attained richness maxima at altitudes between 1200 and 2000 m Schulze (2000) @Brehm2003

The most common altitudinal richness pattern is the hump shape relationship
and the second most common is the monotonical decrease when increasing in
altitude [@Rahbek1995; @Rahbek2005]. However, a search for a consensous
explanatory mechanism is still problematic and could lead to ambiguous
results, specially for insects. Studies based on an incomplete sampling of the
altitudinal gradient extent, ignoring the lowest altitudes, would always lead
into the perception of a monotonical relationship of species
richness and productivity using the four possible combination of the two most
common productivity and altitudinal richness patterns. And, according to the productivity pattern
emploid and the use or not use of standardization area a contrasting pattern
of the relationship between species richness and productivity might result
[@Rahbek2005].

Both patterns can be achieved under the combination of, bias in the sampling
strategy (incomplete or too wide ranges) and the productivity pattern employed
for interpretation (decrease and hump shaped) [@Rahbek2005]. @Willmott2003
also mentions homogeneous sampling effort through an altitudinal gradient as a
source for bias in the sampling strategy of insects, because under equal
sampling more species are prone to be missed where the richness is higher [see
@Colwell1994].

In many studies of diversity through altitudinal gradients the relationship
between richness patterns and abiotic or ecological factors, as the ones
mention above are ussualy the only factors analysed [@Palin2011; @Maveety2011;
@Mccain2004]. But, it is also important to take into account spatial
heterogeneity and to analyze the geographical distances between traps in order
to avoid spatial autocorrelation. In this study the geographical distances
between malaise traps had more explanation power over species composition
changes than differences in altitude and there was not correlation between
altitude and species composition variation when taking away the effect of
geographical distances. Probably, the number of Malaise traps employed was
still small to overcame the spatial heterogeneity of the forest and to reflect
the species turn over along the altitudinal gradient. In the present study
there is a big percentage of species composition variation between traps that
was not explained with geographical or altitudinal differences. As discussed
above precipitation and also solar radiation could have a determinant role on
parasitoids ecology. As mention by @Malhi2010, "over short distances the
compositional shifts are more likely to be environmental rather than been
historical"


Also, the cone shape decreasing area of the montains influence over ecological
mechanisms. Eventhough species with very narrow altitudinal ranges need and
have connection between mountains to extend their latitudinal range, they
still might have higher risk of source sink population dynamics. These biotic
and abiotic mechanisms influence the altitudinal patterns to behave
differently than latitudinal pattenrs and are problably one of the reasons for
the still lack of a uniform altitudinal pattern [@Rahbek2005].

Therefore, this evidence of a strong
host seasonality dependence on the seasonality of the parasitoids, but this host
seasonality can be at the same time influenced by their parasitoids.
Nevertheless, it would be good to take into account that the last mention
study only analysed parasitoids of pest and that most of the biological cycles
of tropical parasitoids and their host remains unknown.

And, when analysing without the spatial
autocorrelation of altitude with geographical distances there was not
correlation between differences in altitude and species composition variation.
I would recoment to future studies to asses the habitat heterogeneity also. This has provee to have relevance ,,,  along with more precise climatic data. 
Precipitation is certanly limiting another very important
factor of the forest, productivity [@Girardin2010] which, as presented above,
can be explaining richness patterns of Pimplinae. 
Also, the altitudinal patterns that I tested here are surrogates of intercorrelated environmental factors and other forces (e.g. stochastic, geographical, historical forces) affecting the species richness patterns in a more complex manner that are difficult to test (@Willig2003).

pattern found there would follow the same
expected pattern of butterflies from lowland Amazonia. As butterlies
abundances are synchronize with precipitation in lowland Amazonia, I proposed
that they would continue the same when increasing in altitude up to 1500 m
where the Pimplinae parasitoids would also synchronize their abundances them
and with the highest precipitation. 

Most of the review literature and the findings in my sampling at 1500 m a.s.l.
agree with the hypotesis that the highest abundances and richness of insects and parasitoid wasps are during
the wet season of seasonal tropical forest. However, there are some cases of
high abundaces on the last months of the dry season that are explained
with different hypothesis. @Shapiro2000 proposed a dry-season refuge
hypothesis in the most humid and elder forest from secundary and surrounding
forest. While @Gauld1987 proposed a host parasitoid dependance on the wasps
parasiting lepidoptera species with their diapause stage synchronize with the
dry season. In our study the low temperature cloud forest at 2800 m a.s.l. let
me to proposed a more climatic explanation that can certainly not reject the
previews hypothesis and in fact could be complementary.

Comparing to previews results of Gauld in seasonal tropical forest, the pimplinae around 1500 synchronize their abundances in the wet season rather than in the dry season. As mention by ,,, perhaps the heavy rains of hiperhumid areas such as Ecuador, and also the cloud mountains of Peru, prolong the inactivity period of the butterlies to one month after the highest precipitation compare to Costa Rica where the highest activity of the butterflies is during the wet season.

However, data from one year only can be misleading as shown by @Wolda1978 with
Fulguroidea from BCI among many other homoptera groups.One year of data may
show in   "One year of data may show in abundance or activity at some time or
other, but a conclusion about whether that peak is seasonal is only warranted
if the pattern can be shown to repeat itself from year to year" @Wolda1988

@Wolda1983 also found a great annual variation in the abundace of insects at
both, temperate and tropical forest, including ichneumonidae.

And, because koinobionts are more host-specific than idiobionts they
would have to synchronize their development with their host [@Barrantes2008].
@Barrantes2008 within the group,,,, one tropical species of Zatypota and one
of Polisphincta were found to be very much synchronize with their host, as
expected, but one host- parasitoid couple was highly seasonal and the other
non seasonal.

@Gauld1987 in Costa Rica, also found similar results. In a
desciduos secondary forest he found the tribes of Pimplini and
Mesostenini to be active during the dry season and he proposed that this would
be due to the highest numbers of their expected host, pupa of Lepidoptera,
during the dry season.

From a tropical garden in Uganda @Owen1970 described a
correlation between abundances of Ichneumonidae, adults of Lepidoptera and one
of the two rain peaks and mentions a similar pattern between Sphingidae and
Ichneumonidae. But, the highest abundance do not correspond with the highest
richness.

the parasitic phenology would depend on the activity of
their host, that can be very seasonal in many hervivorous groups, but it is at
the same time influenced by the seasonality of their parasitoid.

areas, @Owen1991 found a wider temporal
range of parasitoid activity compared to individual host species showing a
more determinant role of niche specificity. While 

At least from registers at non tropical areas, idiobiont parasitoids could have many generations per season (Pyke et al.
1977)???. 

@Shapiro2000
found a strong correlation between moisture and ichneumonid activity
@Shapiro2000
found the differences in Ichneumonidae and also Pimplinae abundances between two tropical forest to correspond with their differences in seasonality. The forest with the highest and longest wet season had the highest abundaces and longest activity. The highest activity and abundance coincide with the rainy months.

The host
seasonality is at the same time affected by the interaction with their
parasitoid. The longevity of the parasitoid larva and their adult stage might
also modify the observed temporal patterns of the host to discreate or
continous generations and, according to [@Godfray1989] independently of
environmental factors?.

Also, @Wolda1983 proposed that the annual
variation in abundace of tropical insects would be the same as insects from
temperate zones.

[@Brehm2008] also mentions that the high diversity of tropical mountais cover
two overlaying peaks, the latitudinal Ecuadorian and the middle elevation peak
of altitudinal gradients.

Tropical mountains embed the diversity peaks of two spatial distribution
patterns, the Ecuadorian peak from latitudinal gradient and the middle 
elevation peak from altitudinal gradients [@Brehm2008]. 

The importance of ecological studies in tropical mountains ... Malhi2010
5 reasons climatic and abiotic key features, species turnover and hotspot,
natural laboratories of environment over ecosystem, monitoring atmosferic
change in tropics and potential refugia for climate change.

and the impact of scale scale effect on altitudinal patterns 
altitud gradients offer the posibility to study hypothesis of global processes at local scale(@Rahbek2005) however

Cloud forest in the tropics are usually isolated providing. isolation may
increase endemisms but that limit dispersion of species making them more
vulnerable to climate change.

Even though the family
Ichneumonidae is a famous exception of the latitudinal pattern, it is not so
for many subfamilies including Pimplinae [@Veijalainen2012]

The altitudinal pattern in Ichneumonidae follows the mid elevation peak
[@Noort2004; @Peck2008]. Also, when analyzed at a finer scale, @Veijalainen
found a mid elevation peak in most of the subfamilies including the biggest
subfamilies and Pimplinae. But those studies were conducted in Mesoamerica and
there is not altitudinal study in the Andes of South America.

 the abundanHowever,  Also
for butterflies, there is phylogenetic evidence that showing the Andean
montane forest as source of recent speciation and proliferation
[@Willmott2001]. However, lack of detailed geographic and elevational range
data make further studies rather difficult [@Hall2005]

ce peak of the
tribe Pimplinae is during the inactive stage of their hosts in the dry season.
The homogeneous temperatures of the tropics allows the parasitoids to be
active and exploit the diapause stage of their hosts .
Temperature, shadow and moisture are the main abiotic factors in the tropics.


 In non specific study, @Gaasch1998 found that
Ichneumonidae subfamilies that attack the same host, Lepidoptera, Symphyta or
Coleoptera hosts, have synchronized peaks of abundance during the active
season.

In a study of an England garden [@Owen1991], many species of
ichneumonidae had a temporal range wider than their hosts. The author proposed
that flight activity of ichneumonids would be determined by their niche
specificity rather than hosts.

For example, birds of tropical mountains modify their elevation
through the year, therefore they also modify the local diversity at several
locations in the altitudinal gradient [@Rosenzweig1995].

DISCUSSION


Killeen demostrated that precipitation patterns over the tropical Andes
are determined by wind patterns, clouds and topography.

Therefore,
geographical distances between the Andes and the adjacent Amazon lowland, as
part of topography must be also influencing precipitation.


Because
organisms are not distributed at random and the environment is estructured
according to different energy inputs, the environment can show patchy
structures or gradients (@Legendre1989). Then ... also recomend to analyse the
natural levels of geographical differences to take into account spatial
heterogeneity and to avoid spatial autocorrelation. In this study geographical
distances had more explanation power over species composition changes than
altitude.



The Pimplinae results can be explained by three ecological hypothesis however
it agrees also with the expected random model without ecological or historical
influences. Nevertheless, a much precise analysis could be achive if the top
(grassland) and lower limits (Amazon lowland) of the valley could be studied.

The factors that can affect the temporal variation of parasitoid activity are
their host phenology , abiotic factors and their life history (@Shapiro2000).

the environmental factor that most likely seem to predict abundance and
richness peaks of butterflies in the lowland tropics, precipitation. 

Hodkinson mentionps a reduction in the number of generation with altitude and ..... propose that there were much shorter life cicles at lower altitudes 

The changes that the insects are supposed to cope and adapt with altitude are:
decrease in temperature
raise of UV radiation
reduction of partial pressure of atmospheric gases. 
precipitation increases with altitude ( or higehr in the middle) but can variate accoding to topography... increase nutrient leaching from the soil where precipitation is high
increase wind speed
,,, (Hodkinson), 

However, altitudinal gradients in the Andes present other environmental
gradients and variances more related to their topography, such as as
moisture,, radiation patterns. In the case of the Kosnipata valley their
environmental characteristics were describe by Raap(ref,).

altitudinal changes in insects with altitude [@Hodkinson2005] and what is expected in tropical areas. Climatic changes in kosnipata valley with altitude. 

There are not many temporal studies of buttlerflies or parasitoid wasps from
upper montane forest in the tropics. One study from Uganda around 1500 m
reported unpredictability on the temporal variation of butterflies with scarse
relation to seasonal patterns.

At 2800 m, higher montane forest, the abundances and richness follow a
different pattern than at lower montane forest. As the altitudinal pattern of
diversity is supposed to mirror the latitudinal pattern (@Rahbek1995) we could
expect that also the temporal pattern at higher altitudes would mirror the
pattern in higher latitudes. In temperate forest the peak of abundances and
activity is during summer because of the extremely cold temperatures that it
can achieve. However, there is not temperature seasonality in the Kosñipata
valley it olny decreases less than 2°C during the coolest months (@Raap2010).
The abundance peak during October coincide with the last month of the dry-wet
season transition (August - October), characterize by the increase of
insolation patterns and changes in humidity and temperature compare to the
previews dry months.

higher populations of the butterfly Aporia crataegi flew in warmer times compare to lower populations [@Merril2008]. 

are the drivers of the altitudinal pattenrs seen trully altitudinal pattenrs or local, regional particularities?? precipitation, PAR, are particularities [@Körner2007]

[@Körner2007] reflects and encourage the use of true geophisical changes over altitudinal gradients as environmental drivers of ecological and evolutive pattenrs instead of local or regional particularities suchas as 

@Körner2007 points aout that studing altitudinal gradients with particular regimes of moisture for example might give results of moisture gradients instead of trully altitudinal effects. 

the altitudinal diversity patterns review by Brehm2003 show: in Asia Lepidoptera peaks between 600 and 1000 m a.s.l. But Geometridae and Arctidae were in Borneo had richness maxima between 1200 and 2000 m. Env. conditions are so harsh that few species can cope with it. 

geophysical environmental changes tied to altitudinal change or local regional particularities. 

precipitation and PAR or solar radiation are both climatic terms not related to 
altitude @Körner2007. 

most important abiotic factors in the tropics, precipitation? ... butterflies
in seasonal forest are more sensityve to env. grad ense precipitation
(Hill2003 menciona), however he found a negative correlation between
individuals of Ragadia makuta (Satirinae) an the precipitation of the month
before the survey. then the larvae were affected if it rain too mach and there
were more butterflies if one month before it was dry.  (perhaps the Pimplinae
are feeding on bigger lepidoptera that would not be damage too easily for
heavy rains).

but temperature is more determinat for butterflies than precipitation,,,,,
temperature data. In [@Checa2009] eventhough the precipitation is reported
previeusly in butteerfly population fluctuation, temperature was even better
in the prediction and it only varies over one degree during the year.



INTRO

How are temporal variation explained?

[Montane species with narrow distribution are
also important to understand patterns of speciation in tropical mountains]
chen 2009 sobre cambio climatico en bosques tropicales

According to Larsen 2011 "perhaps more than 50% of tropical biodiversity in the Andes remain undescribed". brehem 2008 moths. 
endemisms, linked to speciation on montanes. 


seasonal fluctuation of insect fauna from tropics: Wolda & Galindo, 1981; Gauld, 1985b
WOLDA, H. & GALINDO, P., 1981. Population fluctuations of mosquitoes in the non-seasonal tropics. Ecological entomology
GAULD, I. D., 198513. A preliminary survey of Ophioninae (Hymenoptera: Ichneumonidae) of Brunei. Brunei Museum Journal, 6( I) : 169- 188.

Parasitic wasps are thought to have a potential beneficial appliance in biological control of pest on agriculture, but its most attractive characteristics are on conservation due to their high diversity and regulation of insect population on natural areas (Hassel 2000). 

(Shapiro and Pickering 2000) on a tropical study hypothesize moisture as a key
factor on parasitoid activity. Moisture stress can lead parasitoids to abandon
dry  location and search for old grow forest as a refuge for example. Also
water stresses  would have a greater impact on communities from the understory
(parasitoids of  larvae that eat leaves) than to parasitoids searching for
host on the ground  (parasitoids of pupae on ground).  Very little is known of
their community structure,  environmental requirements, host specificity and
geographical ranges .

Lewis et al.(2002) Structure of tropical leaf miners and their parasitoids
community,   found a low level of specialization and low seasonal turnover of
parasitoids compare to leaf miners.  Tropical parasitoids of leaf-miners hosts
fluctuate revealing temporal changes and turnover in their community structure
(Lewis et al. 2002).

Gaasch (1998) Temporal abundance of Ichneumonidae in Georgia was evaluated.
They  hypothesize that host seasonality is more important than other abiotic
factors or  life history characteristics of parasitoids. Peaks of abundances
on subfamilies that attack the same taxon ( ) were synchronized compare to
subfamilies sharing same  life history characteristics ( nocturnal,
koinobiois/idiobiosis, ecto/entdo).

in Gaasch 1998  niche specificity would be more important that the phenology
of individual host  species according to one study on England  (Owen et al.
1981).  She also mention  that according to Gauld 1991 the host stage at which
the parasitoid attack is also  important in the flight activity of
parasitoids. He found that in Pimplinae it would be later in the wet season
because they, idiobionts, mostly attack later stages.

There is a biogeographic bias in the study of parasitic wasps of the
Ichneumonidae family. The knowledge of parasitic wasps from the tropics is
still scarce. The situation is even worst for tropical montains environments.
To my knowledge this is the first study on altitudinal and temporal variation
of  richness and abundance on Ichneumonidae over the tropical  Andes.

The montane forests of the eastern slope of the Andes are the most
biologically and  physically diverse areas of Peru, it posses a
wide range of habitats  and micro habitats [@young1992, 1999].

Also spatial heterogeneity and species turnover over time and spaces are 
important factors governing their abundance and richness. 

Annual cycles in tropics and subtropics. Leigh et al 1982. The ecology of a
tropical forest: seasonal rhythms and long-term changes

Geographic gradients of species is and old intriguing pattern. Altitudinal species gradients was supposed to mirrow latitudinal gradients...

-> The altitudinal distribution of organisms is another component of montane
diversity. 

-> Two main models have been proposed. A decreasing of richness while increasing
in altitude[first discussed in @Terborgh1977] and a mid elevation peak
[@Rahbeck1995]. The first is supported on the reduction with altitude of
productivity, evapotranspiration, temperature and other climatic
characteristics similarly to latitudinal gradient [@Rahbeck1997]. Latelly,
most of the studies agree on a mid elevation peak of species [@Colwell2004]
which is also called, the mid-domain effect [@Colwell1994].This model assumes
a random placement of species and altitudinal range which are constrained by two hard
boundaries, the sea or lower ground at the bottom and the top of the mountain
at the top [@Rahbek1997;(Colwell and Lees 2000)??. This model does not try to
explain a climatic or historic gradient, it is a null model created to exclude
the random geometric aggregation (and see behind what are the most important
factors (temperature, rain, isolation from source areas, etc) working in a
given gradient [@Colwell2004].

       However, the mid-elevation model does not explain the climatic or historic
gradient, it is a null model created to exclude the random geometric
aggregation and see what are the most important factors working behind the
gradient (temperature, rain, isolation from source areas, etc) .

Anu paper: there is a clear compositional gradient in ichneumonid communities along the elevational gradient.

Otra cosa sobre latitudinal gradient,, tal vez conclusion de Anu
[[The ichneumonidae fauna was thought to be one of the few groups that did not
follow the latitudinal pattern of higher diversity in the tropics with some
exceptions (Pimplinae, Ophioninae, ref). There is more evidence that this
believe was bias by lack of sampling effort.]]

Gauld proposed that the South American fauna ...follows the Mesoamerican.... but  south american taxonomic recent studies shows different ichneumondae fauna


DISCUCION


Eventhough the sampling was more less homogeneous, the accumulation curves and
the accuracy of the estimators of species richnes differ at each collecting
point. 

[[if sample sizes are not sufficient, rarefaction will not distinguish
between different richness patterns, because all rarefaction curves converge
at low abundances (tipper 1979)]]

[[]]
The 125 species collected range over an altitudinal gradient of 2 km in
elevation and 26 km in distance. The accumulation curve did not reach an
asymptote at any altitude therefore the species richness of the valley would
be much higher (Fig. 1). Other Andean surveys had similar altitudes recorded
fewer species in the middle elevation but apparently had the same altitudinal
pattern.

In a [national Ichneumonid survey] in Costa Rica the locality with the highest
richness was in the lower montane forest Estacion Cacao (1100 m)
[@Gaston1993]. The number of recorded species was fewer (67 spp.) than San
Pedro (1500 m) with 5 times more sample effort. The submontane forest
Estacion Pitilla had more species (57 spp.) than Tono (800 m) with 6
times more sample effort.

In another national Insect survey in Colombia [@Arias2007, webpage of colombia]
There was very few sampling effort between 900 and 1800 m in the total sampling and the altitudinal pattern between the Andes and the Amazon can not be compare. 

In Colombia an oak forest in the SFF Iguaque (2600 - 2900 m) had much higher diversity, 62 spp., than Wayquechas (2800 m), 32 spp. It also had a remarkable sample intensity 128 MTM comparare to Wayquechas (15 MTM) and the vegetation types was oak forest?? and it involve the two sides of the montain ??? other particularities of the area. 
]]


**Productivity would be determine by soil mineralization rates which are control by temperature at low elevations and soil saturation at higher elevation**

the null model is ... . but all the gradient can not be explained by geometric constrains [@Colwell1994]. ecological evolutionary and historical phenomena must have originated and maintained this richness gradients

*[@Noort2004] also found a mid elevation peak of richness and abundance on Ichneumonidae in Monts Doudou, Gabon (110 - 313 m a.s.l.). tuvo 3 pimplinos]
*In an Andean forest from Colombia the highest divesity of buttesflies was at 1900 m a.s.l. in an altitudinal gradient ranging from 1900 - 3000 m [@Camero2007]. 
*mid elevation in other insects in south america o pimplinae in asia.

The lower tropical montane forest have been proposed to be 'species pumps'
areas where its climatic long-term stability promote speciacion and protect
old species (Fjeldså & Lovett 1997). Using phylogenetic analysis, many genera of Neotropical butterflies proved to have their main center of speciation in
the humid tropical andes [@Willmott2001; @Willmott2003; @Hall2005; @Elias2009; @Rosser2012]. The Andean region and southeast of Brazil were
proposed as main centers of evolution for the South American fauna of
Ichneumonidae [@Porter1980] based on the high number of endemic genera, but no further conclusions can be attempt without phylogenietic analyses. 

Neotropical butterflies *Napeogenes* and *Ithomia* (Ithomiinae) [@Elias2009], Hypanartia (Nymphalinae) [@Willmott2001] and the subtribe Heliconiina [@Rosser2012], *Ithomiola* (Riodinidae) [@Hall2005]. 
montane speciation in Ecuador was important in *Adelpha* species [@Willmott2003]
and the highest richness and endemicity are at 800-900 m a.s.l.
the humid tropical andes is the center of speciation of tanagers (Aves, Thraupini) [@Fjeldså2006]. 

At the eastern slopes of the Ecuadorian Andes the highest
richness of Adelpha coincide with the highest endemic numbers of species
around 800-900 m a.s.l. Still, non prediction can be attemp for montane
parasitoid wasps without further knowledge of endemicity and phylogenetic
analisis.

resource availability 
*In an Andean forest from Colombia the highest divesity of buttesflies was at 1900 m a.s.l. in an altitudinal gradient ranging from 1900 - 3000 m [@Camero2007].

RESULTS

Comparing with climatological data the increase of richness in January at 1500
m conveys with the precipitation peak (Fig. 6). At 2800 m the increase in
October could correspond to the wet-dry season transition characterized by
@Rapp2010 using temperature, humidity and light radiation changes. A
correlation using climatological data was not possible because of the scarse
sampling points.

PREGUNTAS

1.a	Is the spectrum of hosts the same at different altitudes?

**3. Are the abundances of different species in temporal synchrony within altitudes?**