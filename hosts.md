# What are the possible hosts of Pimpla, Neotheronia and Zonopimpla, the main idiob ecto/endo?

# What is the temporal pattern of the possible hosts?

**Checa et al. 2009** Nymphalidae butterflies in Amazonian Yasuni Reseach Station, a forest between 200 -500 m.a.s.l., 26 C temperature, aseasonal but dryer between Aug-Jan with a drop of 425 to 150 mm from July to september. 

-April - September, decrease simmilarty in community
-Constant turn over, less than 13% of specis were all year around.

7 spp, some of the most abundant, had conspicuous peaks and declines through year ---> feeding specialization

  In general, the temporal patterns of abundance in
  butterfly communities may be due to a variation in the
  dynamics of host plants or to a temporal variation in
  larval mortality (Hamer et al. 2005).


-September richness and abundance peak, beggining of dry precipitation period and peak of temperature.
-Richness and ind. at the lowest when precipitation starst to increase, coincide with a low temperature. 
-Temperature explained the daily variation in species and abundances better than precipitation. 

Synchronization between the decrease of precipitation and the increase in the number of species and individuals
 

