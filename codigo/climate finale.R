# EXPORT AS PDF 5 *7 INCH portrait

##climatic comparison
pre_1500 <- c(117.1875,114.0625,218.75,470.3125,384.375,607.8125,681.25)
pre_2800 <- c(65.625,128.125,153.125,239.0625,312.5,356.25,481.25)
hu_1500 <- c(86.4435146444,82.7615062762,72.050209205,77.0711297071,89.7907949791,88.4518828452,92.9707112971)
hu_2800 <- c(87.4476987448,87.6150627615,81.4225941423,85.4393305439,91.4644351464,91.129707113,93.3054393305)
par_1500 <- c(222.268907563,268.9075630252,365.9663865546,353.3613445378,304.2016806723,345.7983193277,271.4285714286)
par_2800 <- c(222.268907563,209.6638655462,285.2941176471,305.462184874,246.218487395,        286.5546218487,217.2268907563)
t_1500 <- c(17.050209205,17.4686192469,19.9790794979,19.769874477,18.8284518828,19.1422594142,18.6192468619)
t_2800 <- c(10.7740585774,10.9832635983,12.2384937238,12.4476987448,12.2384937238,12.4476987448,12.2384937238)
            
#2 graficos climaticos
par(mfrow=c(2,1), oma=c(4, 9, 2, .1), mar=rep(1, 4), cex=.7)
plot(hu_1500, axes=F, ylim=c(0,100), xlab="", ylab="", type="l", col="black")
axis(2, line=0, cex.axis=0.7, ylim=c(0,100), las=1)             
mtext(2,line=2.5,text="Relative humidity (%)", cex=.7)
par(new=TRUE)
plot(hu_2800, axes=F, ylim=c(0,100), xlab="", ylab="", type="l", lty="dashed", lwd=1, col="black")
legend(x=1, y=100, c("humidity"), bty="n", cex=0.7, pch="")

par(new=TRUE)
plot(par_1500, axes=F, ylim=c(0,500), xlab="", ylab="", type="l", col="gray47")
axis(2, line=5, cex.axis=0.7, ylim=c(0,500), las=1)             
mtext(2,line=8, text=expression("PAR (" * mu ~ "E)"), cex=.7)
axis(1, at=1:7, cex.axis=0.7, lab=c("Jul","Aug","Sep","Oct","Nov","Dec","Jan"))
par(new=TRUE)

plot(par_2800, axes=F, ylim=c(0,500), xlab="", ylab="", type="l", lty="dashed", col="gray47")
legend(x=1, y=320, c("PAR"), bty="n", cex=0.7, pch="", text.col="gray47")

plot(pre_1500, axes=F, ylim = c(0,1000), xlab="", ylab="", type="l", col="black")  
axis(2,line=0, cex.axis=0.7, at=250*0:1000, las=1)
mtext(2,line=2.5, text="Precipitation (mm)", cex=.7)
par(new=TRUE)            
plot(pre_2800, axes=F, ylim = c(0,1000), xlab="", ylab="", type="l", lty="dashed",lwd=1, col="black") 
axis(1, at=1:7, cex.axis=0.7, lab=c("Jul","Aug","Sep","Oct","Nov","Dec","Jan"))
legend(x=1, y=260, c("precipitation"), bty="n", cex=0.7, pch="")

par(new=TRUE)
plot(t_1500, axes=F, ylim=c(0,25), xlab="", ylab="", type="l", col="gray47")
axis(2, line=5, cex.axis=0.7, ylim=c(0,25), las=1)             
mtext(2,line=8, text="Temperature (C)", cex=.7)
par(new=TRUE)
plot(t_2800, axes=F, ylim=c(0,20), xlab="", ylab="", type="l", lty="dashed", col="gray47")
legend(x=1, y=17, c("temperature"), bty="n", cex=0.7, pch="", text.col="gray47")

#box outside the plot
par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)
plot(0, 0, type = "n", bty = "n", xaxt = "n", yaxt = "n")
legend("bottom", c("1500 m           ", "2800 m"), xpd = TRUE, horiz = TRUE, inset = c(0,0), bty = "n", pch = c("", ""), lty=c(1,2), cex = .7)

#### 1 grafico climatico FIG 6

par(mar= c(4.1,7.8,2,5))          
plot(pre_1500, axes=F, ylim = c(0,1200), xlab="", ylab="", type="l", col="black")  
axis(2, cex.axis=0.7, at=400*0:1200)
mtext(2,text="Precipitation (mm)", cex=.9, line=2)  

par(new=TRUE)
plot(par_1500, axes=F, ylim=c(0,500), xlab="", ylab="", type="l", col="red")
axis(4, cex.axis=0.7, ylim=c(0,500))             
mtext(4,text="PAR (μE)", cex=.9, line=2.2)
axis(1, at=1:7, cex.axis=0.8, lab=c("Jul","Aug","Sep","Oct","Nov","Dec","Jan"))

par(new=TRUE)
plot(hu_1500, axes=F, ylim=c(0,100), xlab="", ylab="", type="l", col="blue")
axis(2, cex.axis=0.7, ylim=c(0,100), line=3.4)             
mtext(2,text="Relative humidity (%)", cex=.9, line=5.4)

par(new=TRUE)            
plot(pre_2800, axes=F, ylim = c(0,1200), xlab="", ylab="", type="l", lty="dashed",lwd=2, col="black")  

par(new=TRUE)
plot(par_2800, axes=F, ylim=c(0,500), xlab="", ylab="", type="l", lty="dashed", lwd=2, col="red")

par(new=TRUE)
plot(hu_2800, axes=F, ylim=c(0,100), xlab="", ylab="", type="l", lty="dashed", lwd=2, col="blue")

legend(x=.5, y=85, c("Humidity"), bty="n", cex=0.8, text.col="blue")
legend(x=.5, y=60, c("PAR"), bty="n", cex=0.8, text.col="red")
legend(x=.5, y=25, c("Precipitation"), bty="n", cex=0.8)

#box
legend(x=5.7, y=86, legend=c("1500 m","2800 m"), text.width=.7:0.1, y.intersp=1, lty=c(1,2), cex=0.6)

#temp
par(new=TRUE)
plot(t_1500, axes=F, ylim=c(0,50), xlab="", ylab="", type="l", col="brown")
axis(2, cex.axis=0.7, ylim=c(0,20), line=3.4)             
mtext(2,text="Relative humidity (%)", cex=.9, line=5.4)
par(new=TRUE)
plot(t_2800, axes=F, ylim=c(0,20), xlab="", ylab="", type="l", lty="dashed", col="brown")

