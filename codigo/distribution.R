setwd("~/Desktop/tesis/datos")
pim <- read.csv("pim.csv", header = TRUE, sep = ",", nrows = 1312, stringsAsFactors = TRUE)
pim$altitud <- as.factor(pim$altitud)
trap <- which(pim$trampas != "manual")  #indice de las filas que no son "manual"
trapdata <- pim[trap,]     #dataframe del indice

species2 <- c(trapdata$especie)
species2.freq = table(species2)
species2.freq    #abundancia por specie
length(species2.freq)   #numero total de especies, solo trampas
abundancia<-as.vector(species2.freq) # vector de abundancia por especie, solo trampas

library("fitdistrplus")
library("survival")
library("splines")

fit_exp <- fitdist(data=abundancia, distr="exp", method=c("mme"))
fit_gamma <- fitdist(data=abundancia, distr="gamma", method=c("mme"))
plot(fit_gamma)
summary(fit_gamma)
fit_lognormal <- fitdist(data=abundancia, distr="lnorm", method=c("mme"))
fit_pois <- fitdist(data=abundancia, distr="pois", method=c("mme"))