setwd("~/Desktop/tesis/datos")
pim <- read.csv("pim.csv", header = TRUE, sep = ",", stringsAsFactors = TRUE)
#calcula los diferentes niveles y puede clasificar vectores de otras columnas de acuerdo esta informacion
str(pim)
names(x)
row.names(x)

t <- split(pim, pim$trampas)


library(MASS)
library(vegan)

trampas <- c(17,39,36,29,6,39,22,11,0,2,10,0,9,31,25,4,44,3,0,50,10,5,6,31,0,37,33,51,51,35,42,69,24,52,53,0,3,4,0,0,3,5,0,2,16,18,4,5,3,6,8,7,75,75)
sd(trampas)
mean(trampas)
median(trampas)
boxplot(trampas)
 
dias <- c( 35,60,34,48,27)
sd(dias)
mean(dias)
boxplot(dias)

locality = pim$localidad

locality.freq = table(locality)
locality.freq
locality.freq / nrow(pim) #freaquencia relativa
barplot(locality.freq)
pie(locality.freq)

summary(pim)
altitudFactor <- as.factor(pim$altitud)
barplot(summary(pim$altitudFactor))

altitude = pim$altitud
altitude.freq = table(altitude)
altitude.freq
barplot(altitude.freq, ylim = c(0,600), las = 1, ylab ="abundance", xlab ="altitude") 
par(new=TRUE)

plot( ..., axes=FALSE, ... )   <-- plot our second plot, but don't touch the axes
axis(4, pretty(c(0, 1.1*max(yahoo2$close))), 

tono_altitude <- altitude == "800"
tono_data = data[tono_altitude, ]
tono.freq = table(tono_data$"especie")
tono.freq 
tono_data$"especie"

tapply(data$especie, data$altitud, mean)