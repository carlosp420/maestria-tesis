\select@language {english}
\contentsline {section}{1. Introduction}{1}{section*.3}
\contentsline {subsection}{1.1 The study organism}{1}{section*.4}
\contentsline {subsection}{1.2 Tropical insect seasonality and parasitoid activity}{3}{section*.5}
\contentsline {subsection}{1.3 Altitudinal richness patterns}{5}{section*.6}
\contentsline {subsection}{1.4 Insect diversity and conservation of tropical Andes}{7}{section*.7}
\contentsline {subsection}{1.5 Aim of the study}{8}{section*.8}
\contentsline {subsection}{1.6 Questions of the study}{8}{section*.9}
\contentsline {section}{2. Materials and methods}{9}{section*.10}
\contentsline {subsection}{2.1. Study area and data}{9}{section*.11}
\contentsline {subsection}{2.2 Sampling}{11}{section*.13}
\contentsline {subsection}{2.3 Analysis}{11}{section*.14}
\contentsline {section}{3. Results}{13}{section*.15}
\contentsline {subsection}{3.1 Altitudinal variation}{13}{section*.21}
\contentsline {subsection}{3.2 Temporal variation}{19}{section*.25}
\contentsline {section}{4. Discussion}{22}{section*.27}
\contentsline {subsection}{4.1 Altitudinal patterns}{22}{section*.28}
\contentsline {subsection}{4.2 Temporal patterns}{25}{section*.29}
\contentsline {section}{5. Bibliograhy}{29}{section*.31}
\contentsline {section}{6. Appendix}{36}{section*.32}
